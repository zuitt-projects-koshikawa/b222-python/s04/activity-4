from abc import ABC, abstractclassmethod

class Animal(object):
    def __init__ (self, name, breed, age):
        self.name = name
        self.breed = breed
        self.age = age
        
    @abstractclassmethod
    def eat(self):
        pass
    
    @abstractclassmethod
    def make_sound(self):
        pass
    
    def get_name(self):
        print(f"Name: {self.name}")
        
    def set_name(self, name):
        self.name = name
    
    def get_age(self):
        print(f"Age: {self.age}")
        
    def set_age(self, age):
        self.age = age
        
    def get_breed(self):
        print(f"Breed: {self.breed}")
        
    def set_breed(self, breed):
        self.breed = breed
        
class Cat(Animal):                
    def eat(self, food):
        self.food = food
        print(f"Cat eats {self.food}")
    
    def make_sound(self):
        print("Meow! meow!")
    
    def call(self):
        print(f"Here {self.name}!")
        
class Dog(Animal):   
    def eat(self, food):
        self.food = food
        print(f"Dog eats {self.food}")
    
    def make_sound(self):
        print("Bark! Woof! Arf! Arf!")
    
    def call(self):
        print(f"Here {self.name}!")
                

        
cat1 = Cat("Tom", "Shorthair", 3)
dog1 = Dog("Parky", "Bulldog", 2)

cat1.eat("Tuna")
cat1.make_sound()   
cat1.call()

dog1.eat("Steak")
dog1.make_sound()   
dog1.call()